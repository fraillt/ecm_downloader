### Kaip veikia scriptas

Paleidziant scripta reikia nurodyti nuo/iki kurio failo noresi downloadint.
Pvz: `python .\main.py 3 20` downloadins viska nuo 3-io (ismtinai) iki 20-to pasirinkto failo imtinai.
Pirmiausia jis pasirenka faila (pajudina pele iki reikiamo draiverio/failo ir paklikina), 
tada paspaudzia "confirm" mygtuka, ir laukia kazkiek laiko... toliau tas pats su sekanciu failu.

### Pries paleidziant pirma karta

* Instaliuok `python` (is windows store)
* pasileisk `cmd` ar `powershell`, nueik i sita kataloga, ir suinstaliuok reikalingus `pip` paketus
su `pip install -r .\requirements.txt` komanda.
