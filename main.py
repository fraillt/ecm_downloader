import pygetwindow as gw
import pyautogui
import sys
import time


WINDOW = "ECM Titanium" # name of application 
DOWNLOAD_LIST_LEFT_TOP = 300, 200 # pixel x,y from left top
CONFIRM_BUTTON_LEFT_TOP = 500, 500
WAIT_UNTIL_DOWNLOADED = 5.5 # seconds until download finishes
DIFF_BETWEEN_LIST_ITEMS = 10 # pixels in Y axis

def find_window(window_title):
    try:
        window = gw.getWindowsWithTitle(window_title)[0]
        return window
    except IndexError:
        return None
    
def select_file(wnd, index):
    move_and_click(wnd, DOWNLOAD_LIST_LEFT_TOP[0], DOWNLOAD_LIST_LEFT_TOP[1] + index * DIFF_BETWEEN_LIST_ITEMS, 0.2)

def click_download_and_wait(wnd):
    move_and_click(wnd, CONFIRM_BUTTON_LEFT_TOP[0], CONFIRM_BUTTON_LEFT_TOP[1], 0.2)
    time.sleep(WAIT_UNTIL_DOWNLOADED)
    
def move_and_click(wnd, x, y, delay):
    left = wnd.left + x
    top = wnd.top + y
    pyautogui.moveTo(left, top, duration=delay)
    pyautogui.click()

def download_file(wnd, index):
    print(f"download: {index}")
    select_file(wnd, index)
    click_download_and_wait(wnd)    

# skip script name
args = sys.argv[1:]

assert len(args) == 2, f"provide range to download: 'start' and 'end'"

for index in range(int(args[0]), int(args[1])):
    wnd = find_window(WINDOW)
    if wnd:
        download_file(wnd, index)
    else:
        print(f"Window {WINDOW } not found!!!")
        break


    